# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Tax Code Type',
    'name_de_DE': 'Buchhaltung Steuerkennziffern Typ',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Types for Tax Codes
    - Adds Types for Tax Codes
''',
    'description_de_DE': '''Typen für Steuerkennziffern
    - Fügt Typen zu Steuerkennziffern hinzu
''',
    'depends': [
        'account',
    ],
    'xml': [
        'tax.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
