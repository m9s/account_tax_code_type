#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, PYSONEncoder


class CodeTemplate(ModelSQL, ModelView):
    _name = 'account.tax.code.template'

    def __init__(self):
        super(CodeTemplate, self).__init__()
        self.parent = copy.copy(self.parent)
        if self.parent.domain is None:
            self.parent.domain = []
        domain_type = ['OR', ('type', '=', Eval('type')), ('type', '=', 'view')]
        if PYSONEncoder().encode(domain_type) not in \
                PYSONEncoder().encode(self.parent.domain):
            self.parent.domain += [domain_type]
        if 'type' not in self.parent.depends:
            self.parent.depends = copy.copy(self.parent.depends)
            self.parent.depends.append('type')
        self._reset_columns()

    type = fields.Selection([
        ('tax', 'Tax'),
        ('base', 'Base'),
        ('view', 'View')
        ], 'Type', required=True)

    def _get_tax_code_value(self, template, code=None):
        res = super(CodeTemplate, self)._get_tax_code_value(template)
        if not code or code.type != template.type:
            res['type'] = template.type
        return res

CodeTemplate()


class Code(ModelSQL, ModelView):
    _name = 'account.tax.code'

    def __init__(self):
        super(Code, self).__init__()
        self.parent = copy.copy(self.parent)
        if self.parent.domain is None:
            self.parent.domain = []
        domain_type = ['OR', ('type', '=', Eval('type')), ('type', '=', 'view')]
        if PYSONEncoder().encode(domain_type) not in \
                PYSONEncoder().encode(self.parent.domain):
            self.parent.domain += [domain_type]
        if 'type' not in self.parent.depends:
            self.parent.depends = copy.copy(self.parent.depends)
            self.parent.depends.append('type')

        self._reset_columns()

    type = fields.Selection([
        ('tax', 'Tax'),
        ('base', 'Base'),
        ('view', 'View')
        ], 'Type', required=True)

Code()
